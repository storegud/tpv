from openerp import fields, models

class res_company(models.Model):
    _inherit = 'res.company'
    
    doc_number = fields.Char(related='partner_id.doc_number' ,string='RUC')