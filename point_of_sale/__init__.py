# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import account_bank_statement
import controllers
import document_peru
import point_of_sale
import report
import res_users
import res_partner
import res_company
import wizard
import res_config
