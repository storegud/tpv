# -*- encoding: utf-8 -*-
##############################################################################
#    
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Storegud Corporation SAC (<http://www.storegud.com).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.     
#
##############################################################################

from openerp import api, fields, models
from PIL import Image
import requests
import pytesseract
from bs4 import BeautifulSoup
import StringIO
from openerp.osv import osv
from openerp.tools.translate import _

class table_2(models.Model):
    _name='table.2'
    
    name = fields.Char('Descripción')
    code = fields.Char('Código', size=2)
    
class res_partner(models.Model):
    _inherit = 'res.partner'
    
    doc_type = fields.Many2one ('table.2', 'Document type')
    doc_number = fields.Char('Document Number',select=1)
    
    @api.one
    def validate_document(self):
        if self.doc_type.name=='DOCUMENTO NACIONAL DE IDENTIDAD (DNI)' and len(self.doc_number)!=8:
            raise Warning(_('El DNI debe constar de 8 dígitos'))
               
    @api.onchange('doc_number')
    def onchange_doc_number(self):

        if self.doc_number:
            tdireccion=""
            tnombre=""
            doc_number=self.doc_number
            if self.doc_type.code == '6':
                #verify RUC
                factor = '5432765432'
                sum = 0
                dig_check = False
                    
                if len(doc_number) != 11:
                    raise osv.except_osv(
                        _('Error'),
                        _(doc_number+" RUC incorrecto"))
                try:
                    int(doc_number)
                except ValueError:
                    raise osv.except_osv(
                        _('Error'),
                        _(doc_number+" RUC debe contener solo numeros enteros"))
                                 
                for f in range(0,10):
                    sum += int(factor[f]) * int(doc_number[f])
                        
                subtraction = 11 - (sum % 11)
                if subtraction == 10:
                    dig_check = 0
                elif subtraction == 11:
                    dig_check = 1
                else:
                    dig_check = subtraction


                if int(doc_number[10]) == dig_check:
                    print '_%s_' % doc_number, type(doc_number)
                else:
                    raise osv.except_osv(
                        _('Error'),
                        _(doc_number+" El RUC ingresado no es correcto"))

            else:
                return False
        else:
            return False
        return False

    